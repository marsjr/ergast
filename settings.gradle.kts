rootProject.name = "formula-one"

include(":worker-notification", ":client-ergast", ":worker-api", ":worker-redis")
