### API FORMULA-ONE

 API desenvolvida para consumir informações referentes à fórmula 1 a partir dos serviços 
 disponibilizados na [ergast](https://ergast.com/mrd/).

---

### :classical_building: - Desenho da solução

![desenho-solucao](desenho.png)

---

### Introdução 

 <details>
  <summary>Tecnologias</summary>

  <ol>
    <li>Kotlin</li>
    <li>Spring Boot</li>
    <li>Spring Cloud AWS</li>
    <li>Spring Cache</li>
  </ol>
</details>

---

### Configuração da aplicação 

O propósito desta seção é auxiliar-vos em como configurar a aplicação 
que consigamos executá-la com êxito. Contudo, sigamos os seguintes passos:

---
#### 1. Criar configurações na AWS console

A nossa aplicação contém configurações de _SNS_ (Amazon Simple Notification Service)
e _SQS_ (Amazon Simple Queue Service). E para o funcionamento das notificações
precisamos configurar os devidos serviços. Para não nos extendermos em uma explicação
exaustiva, propomos configurar os serviços através da leitura da seguinte vídeo-aula:

* [Configuração SNS/SQS no console](https://www.youtube.com/watch?v=cJHuEpwlOQM)

Após a configuração — dos serviços — via console, daremos continuidade a configuração
da nossa aplicação mediator, nomeada de **worker-api**.

1. Configure as seguintes variáveis de ambiente, conforme ilustrado
  
![img.png](img.png)

2. Após isto, o módulo **worker-api** estará pronto.

---

#### 2. Configuração submodulo _worker-notification_

No submodulo do **worker-notification** procure pelo arquivo de configuração
chamado _application.yml_ e insira as credenciais e endpoint de acesso aos serviços.



Obs.: No futuro, criaremos uma estratégia de expor credenciais via _secrets_
do github/gitlab, a fim de tornar nossa aplicação mais segura.

---
#### 3. Configuração do Docker
Para conseguirmos armazenar todas as informações requisitadas pela API,
precisaremos configurar o serviço de _Cache_ - o Redis -. Para isto, é necessário,
termos o Docker instalado na nossa máquina. Caso não tenha, acesse o seguinte
tutorial e configure o Docker na sua máquina.

* [Configuração - Docker](https://docs.docker.com/get-started/)

Após termos configurado o Docker em nossa máquina, precisaremos rodar o seguinte
comando - para criar o _container_ - na nossa máquina:
<p style="text-align: center;">
    <b>docker-compose -f docker-compose.yml up -d</b>
</p>


Obs.: Antes de executar o seguinte comando, confirme, se você já se encontra 
no _path_ onde o arquivo `docker-compose.yml` está.

Após executar o comando, você irá se deparar com o seguinte comportamento

![img_2.png](img_2.png)

o que significa dizer que o serviço _redis_ já se encontra disponível para utilização

---
#### Como acessar o CLI do Redis?

* Execute o seguinte comando: ```docker exec -it worker-redis_redis_1 /bin/sh```
* Logo em seguida, execute: `redis-cli`
* Por fim, execute: `auth MDNcVb924a`

Para acessar os dados armazenados basta rodar o seguinte comando:
* `keys *`- representa um select -
---
#### 5. Executar a aplicação

Após finalizarmos todas as configurações da aplicação, precisaremos seguir 
uma ordem de execução.

1. Execute o _client-ergast_ - client responsável por disponibilizar os endpoints
do ergast -;
2. Logo em seguida, execute o _worker-api_;
3. Por fim, escolha a sua ferramenta de requisição preferida, e começe utilizar a aplicação.
---
### 6. Roadmap 

##### 6.1 - AWS
- Amazon Cognito;
- Amazon CloudWatch;
- CI/CD.

##### 6.2 - Spring 
- Spring Security;
- HATEOAS com Feign Client;
- Autenticação e Autorização.

###### 6.3 - Features

- CRUD de resultados de corridas;
- CRUD de informações dos resultados;
- CRUD de classificação;
- CRUD de Programação da corrida.

##### 6.4 - Testes
- Criar testes de integração e unitários;

##### 6.6 - Observabilidade e Monitoramento
- Monitoramento de infraestrutura com Grafana;
- Gerenciamento e centralização de logs (Fluentd ou Kibana).

