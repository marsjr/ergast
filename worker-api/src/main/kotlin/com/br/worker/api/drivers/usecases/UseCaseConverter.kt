package com.br.worker.api.drivers.usecases

import com.br.worker.api.drivers.service.dto.DriverServiceDTO
import com.br.worker.api.drivers.usecases.dto.DriverUseCaseDTO
import java.util.stream.Stream

//fun DriverServiceDTO.toDriveUseCaseDTO() : DriverUseCaseDTO = DriverUseCaseDTO(
//    driverId = this.driverId,
//    url = this.url,
//    permanentNumber = this.permanentNumber,
//    givenName = this.givenName,
//    familyName = this.familyName,
//    dateOfBirth = dateOfBirth,
//    nationality = this.nationality
//)

fun toDriverUseCaseDTO(driverId: List<DriverServiceDTO>?): List<DriverUseCaseDTO>? {
    return driverId?.map {
        DriverUseCaseDTO(
            driverId = it.driverId,
            url = it.url,
            permanentNumber = it.permanentNumber,
            givenName = it.givenName,
            familyName = it.familyName,
            dateOfBirth = it.dateOfBirth,
            nationality = it.nationality
        )
    }
}