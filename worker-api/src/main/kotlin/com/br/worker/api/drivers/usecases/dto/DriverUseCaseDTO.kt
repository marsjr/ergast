package com.br.worker.api.drivers.usecases.dto

data class DriverUseCaseDTO(
    val driverId: String?,

    val url: String?,

    val permanentNumber: String?,

    val givenName: String?,

    val familyName: String?,

    val dateOfBirth: String?,

    val nationality: String?
)