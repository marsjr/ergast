package com.br.worker.api.drivers.service

import com.br.ergastapi.service.DriverFeignService
import com.br.worker.api.drivers.service.dto.DriverServiceDTO
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class DriverServiceImpl(private val driverFeignService: DriverFeignService): DriverService {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun getDriverById(driverId: String): List<DriverServiceDTO>? {
        logger.info("[DriverService] Worker-API: getDriverByid: {}", driverId)
        return toDriverServiceDTO(driverFeignService.getDriverById(driverId))
    }
}

