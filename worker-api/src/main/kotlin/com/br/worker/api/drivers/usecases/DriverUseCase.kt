package com.br.worker.api.drivers.usecases

import com.br.worker.api.drivers.usecases.dto.DriverUseCaseDTO

interface DriverUseCase {
    fun getDriverById(driverId: String): List<DriverUseCaseDTO>?
}
