package com.br.worker.api.redis.service

import com.br.worker.api.drivers.usecases.dto.DriverUseCaseDTO

interface DriverDTOService {

    fun save(driverResponseDTO: List<DriverUseCaseDTO>?, driverId: String)
}
