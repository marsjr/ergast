package com.br.worker.api.drivers.service

interface NotificationService {
    fun sendNotification(entity: String)
}