package com.br.worker.api.drivers.service

import com.br.worker.api.drivers.service.dto.DriverServiceDTO

interface DriverService {
    fun getDriverById(driverId: String) : List<DriverServiceDTO>?
}