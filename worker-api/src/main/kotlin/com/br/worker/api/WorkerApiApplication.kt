package com.br.worker.api

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching

@SpringBootApplication(scanBasePackages = ["com.br.ergastapi", "com.br.aws.sqs", "com.br.worker.api.redis", "com.br.worker.api.drivers"])
@EnableCaching
class WorkerApiApplication

fun main(args: Array<String>) { runApplication<WorkerApiApplication>(*args) }
