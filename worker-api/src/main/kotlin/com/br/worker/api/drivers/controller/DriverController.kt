package com.br.worker.api.drivers.controller

import com.br.aws.sqs.sender.MessageSender
import com.br.worker.api.drivers.controller.dto.DriverResponseDTO
import com.br.worker.api.drivers.usecases.DriverUseCase
import com.br.worker.api.redis.service.DriverDTOService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping


@Controller
@RequestMapping("/api/v1/drivers")
class DriverController(private val driverUseCase: DriverUseCase, private val messageSender: MessageSender, private val driverDTOService: DriverDTOService) {

  private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

  @GetMapping("/{driverId}")
  fun getDriverById(@PathVariable driverId: String): ResponseEntity<List<DriverResponseDTO>>? {
    logger.info("[DriverController] - Request to return Driver por driverId")
    driverDTOService.save(driverUseCase.getDriverById(driverId), driverId)
    messageSender.sendMessage(driverUseCase.getDriverById(driverId).toString())
    return ResponseEntity.ok(toDriverResponseDTO(driverUseCase.getDriverById(driverId)))
  }
}

