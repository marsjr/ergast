package com.br.worker.api.drivers.usecases

import com.br.worker.api.drivers.service.DriverService
import com.br.worker.api.drivers.usecases.dto.DriverUseCaseDTO
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import java.util.stream.Collectors.toList

@Component
class DriverUseCaseImpl(private val driverService: DriverService): DriverUseCase {
    override fun getDriverById(driverId: String): List<DriverUseCaseDTO>? =
        toDriverUseCaseDTO(driverService.getDriverById(driverId))
}
