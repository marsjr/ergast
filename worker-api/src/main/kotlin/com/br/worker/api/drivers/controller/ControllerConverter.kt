package com.br.worker.api.drivers.controller

import com.br.worker.api.drivers.controller.dto.DriverResponseDTO
import com.br.worker.api.drivers.usecases.dto.DriverUseCaseDTO

fun DriverUseCaseDTO.toDriverResponseDTO(): DriverResponseDTO = DriverResponseDTO(
    driverId = this.driverId,
    url = this.url,
    permanentNumber = this.permanentNumber,
    givenName = this.givenName,
    familyName = this.familyName,
    dateOfBirth = dateOfBirth,
    nationality = this.nationality
)

fun toDriverResponseDTO(driverId: List<DriverUseCaseDTO>?): List<DriverResponseDTO>? {
    return driverId?.map {
        DriverResponseDTO(
            driverId = it.driverId,
            url = it.url,
            permanentNumber = it.permanentNumber,
            givenName = it.givenName,
            familyName = it.familyName,
            dateOfBirth = it.dateOfBirth,
            nationality = it.nationality
        )
    }
}
