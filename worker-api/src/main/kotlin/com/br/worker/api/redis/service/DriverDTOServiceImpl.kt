package com.br.worker.api.redis.service

import com.br.worker.api.drivers.usecases.dto.DriverUseCaseDTO
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

@Service
class DriverDTOServiceImpl: DriverDTOService {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    @Cacheable(value = ["drivers"], key = "#driverId")
    override fun save(driverResponseDTO: List<DriverUseCaseDTO>?, driverId: String) {
        logger.info("[Redis Service]: Salvando em cache!")
    }
}
