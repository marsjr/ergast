package com.br.worker.api.drivers.service

import com.br.ergastapi.service.dto.DriverDTO
import com.br.worker.api.drivers.service.dto.DriverServiceDTO

fun toDriverServiceDTO(driverById: MutableList<DriverDTO>?): List<DriverServiceDTO>? {
    return driverById?.map {
        DriverServiceDTO(
            driverId = it.driverId,
            url = it.url,
            permanentNumber = it.permanentNumber,
            givenName = it.givenName,
            familyName = it.familyName,
            dateOfBirth = it.dateOfBirth,
            nationality = it.nationality
        )
    }
}
