package com.br.worker.api.drivers.controller.dto

import org.springframework.cache.annotation.Cacheable

@Cacheable(value = ["drivers"])
data class DriverResponseDTO(
    val driverId: String?,

    val url: String?,

    val permanentNumber: String?,

    val givenName: String?,

    val familyName: String?,

    val dateOfBirth: String?,

    val nationality: String?
)

