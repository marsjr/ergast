package com.br.worker.api.drivers.service

import com.br.aws.sqs.sender.MessageSender
import org.springframework.stereotype.Service

@Service
class NotificationServiceImpl(private val messageSender: MessageSender): NotificationService {

    override fun sendNotification(entity: String) {
        messageSender.sendMessage(entity)
    }
}