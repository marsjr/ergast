package com.br.aws.sqs.utils

class ConstantUtils {
    companion object {
        const val PREFIX_PROTOCOL_SQS = "sqs"
        const val PREFIX_PROTOCOL_EMAIL = "email"
        const val SQS_MESSAGE = "SQS send message sucessfully!"
        const val EMAIL_MESSAGE = "Email subscribe sucessfully!"
        const val SUBSCRIBRE_MESSAGE = "Send subscribre notification to SQS!"
    }
}