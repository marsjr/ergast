package com.br.aws.sqs

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication(scanBasePackages = ["com.workercache.redis", "com.br.aws.sqs"])
class WorkerNotificationApplication

fun main(args: Array<String>) { runApplication<WorkerNotificationApplication>(*args) }
