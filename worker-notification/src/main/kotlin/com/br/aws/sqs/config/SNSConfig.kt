package com.br.aws.sqs.config

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.regions.Regions
import com.amazonaws.services.sns.AmazonSNS
import com.amazonaws.services.sns.AmazonSNSClientBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Primary

@Configuration
class SNSConfig {

    @Value("\${cloud.aws.credentials.access-key}")
    lateinit var accessKey: String

    @Value("\${cloud.aws.credentials.secret-key}")
    lateinit var secretKey: String

    @Bean
    @Primary
    fun amazonSNSClient(): AmazonSNS? {

        val credentials = BasicAWSCredentials(accessKey, secretKey)
        return AmazonSNSClientBuilder
            .standard()
            .withRegion(Regions.US_WEST_2)
            .withCredentials(AWSStaticCredentialsProvider(credentials))
            .build()
    }
}
