package com.br.aws.sqs.sender

import com.amazonaws.services.sns.AmazonSNSClient
import com.amazonaws.services.sns.model.PublishRequest
import com.amazonaws.services.sns.model.SubscribeRequest
import com.br.aws.sqs.utils.ConstantUtils
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.cache.annotation.Cacheable
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MessageSender(private val amazonSNSClient: AmazonSNSClient) {

    @Value("\${SNS_ARN}")
    lateinit var SNS_ARN: String

    @Value("\${SQS_ARN}")
    lateinit var SQS_ARN: String

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    @PostMapping("/subscribe/sqs")
    fun subscribeNotificationToSQS(): String {
        logger.info("[MessageSender]: Subscribe Notification to SQS!")
        val subscribre = SubscribeRequest(SNS_ARN, ConstantUtils.PREFIX_PROTOCOL_SQS, SQS_ARN)
        amazonSNSClient.subscribe(subscribre)
        return ConstantUtils.SUBSCRIBRE_MESSAGE
    }

    //@PostMapping("/send/{message}")
    fun sendMessage(@PathVariable(value = "message") message: String): String {
        logger.info("[MessageSender]: Send message!")
        val publish = PublishRequest(SNS_ARN, message, "[MENSAGERIA - JS]")
        amazonSNSClient.publish(publish)
        return ConstantUtils.SQS_MESSAGE
    }

    @PostMapping("/subscribe/{email}")
    fun subscribeNotificationToEmail(@PathVariable(value = "email") email: String): String {
        logger.info("[MessageSender]: Send subscribre to Email!")
        val subscribre = SubscribeRequest(SNS_ARN, ConstantUtils.PREFIX_PROTOCOL_EMAIL, email)
        amazonSNSClient.subscribe(subscribre)
        return ConstantUtils.EMAIL_MESSAGE
    }
}
