package com.br.aws.sqs.receiver

import com.workercache.redis.service.ReceiverService
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.cloud.aws.messaging.listener.SqsMessageDeletionPolicy
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener
import org.springframework.stereotype.Service

@Service
class MessageReceiver(private val receiverService: ReceiverService) {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    @SqsListener(value = ["sqs-queue-west"], deletionPolicy = SqsMessageDeletionPolicy.ON_SUCCESS)
    fun receiver(message: String?) {
        receiverService.receiverMessage(message.toString())
        logger.info("Message received {}", message)
    }
}

