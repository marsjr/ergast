package com.workercache.redis.service

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class ReceiverServiceImpl: ReceiverService {

    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun receiverMessage(message: String) {
        logger.info("[ReceiverService] - receiverMessage: Messagem incluída no cache")
    }
}