package com.workercache.redis.controller

import org.springframework.cache.annotation.CacheEvict
import org.springframework.cache.annotation.Cacheable
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HelloController {

    @Cacheable("hello")
    @GetMapping("/hello")
    fun hello(): String {
        println("Sem Cache")
        return "Hello World"
    }

    @CacheEvict("hello")
    @GetMapping("/cancel")
    fun cancel(): String {
        println("Limpando o cache!")
        return "Cache cancelado na tela"
    }
}